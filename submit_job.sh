#!/usr/bin/env bash
# vim: ts=4:sw=4
# set -x

CUR_DIR=$(realpath $(dirname "$0"))

S3ROOT="s3://interpreta-developer-sandbox/slava"

# ../../SPLIT_DIR/splitDir.sh  -s s3://interpreta-developer-sandbox/slava/data.tar.bz2 -d s3://interpreta-developer-sandbox/slava/split-data
# TODO:
# 	JOB_ID: increment for each subdir.
# 	DATADIR: point to a subdir in split-dir.

# Standard function to print an error and exit with a failing return code
error_exit () {
  echo "${BASENAME} - ${1}" >&2
  exit 1
}

function gen_json {
	local START_JOB_ID=$1
	local SIZE=$2
	jq \
		--arg START_JOB_ID $START_JOB_ID \
		--arg SIZE $SIZE \
		--arg CFGDIR cfg \
		--arg MYSQL_HOST intp-dev-uw2-mysql-01.cdh0yy3dmvtr.us-west-2.rds.amazonaws.com \
		--arg MYSQL_DB slava_sample \
		--arg NUM_THREADS 5 \
		--arg KBDIR ICLN/data/kb \
		--arg MONGO_HOST dev-cluster0-pl-0-us-west-2.x9eqz.mongodb.net \
		--arg MONGO_DB slava_sample \
		--null-input '
	{
	"jobName": "loader-arr",
	"jobQueue": "queue-loader",
	"arrayProperties": {
    	"size": $SIZE | tonumber
  	},
	"jobDefinition": "fetch_and_run_interpreta_build_img:1",
	"containerOverrides":
	{
	"command":
	[
		"rds_loader.sh",
		"--rds-par",
		"--configFilesDir=" + $CFGDIR,
		"--mysqlDbName=" + $MYSQL_DB,
		"--mysqlHostName=" + $MYSQL_HOST,
		"--numThreads=" + $NUM_THREADS,
		"--mongo-par",
		"--kbPath=" + $KBDIR,
		"--configFilesDir=" + $CFGDIR,
		"--mysqlHostName=" + $MYSQL_HOST,
		"--mysqlDbName=" + $MYSQL_DB,
		"--mongoHostName=" + $MONGO_HOST,
		"--mongoDbName=" + $MONGO_DB,
		"--numThreads=" + $NUM_THREADS,
		"--doCompoundIndex",
		"--rerun",
		"--doUniqueIndexCheck",
		"--doCountValidation",
		"--errorPrintThreshold=20",
		"--ignoreNullUniqueKeys"

	],
	"environment":
	[
		{
			"name": "BATCH_FILE_S3_URL",
			"value": "s3://interpreta-developer-sandbox/slava/rds_loader.sh"
		},
		{
			"name": "BATCH_FILE_TYPE",
			"value": "script"
		},
		{
			"name": "START_JOB_ID",
			"value": $START_JOB_ID
		}
	]
	}}'
}


function make_TMPDIR {
	mkdir -p TMP
	TMPDIR="$(mktemp -d -p TMP -t data.XXXXXXXXX)" || error_exit "Failed to create temp directory."
}

make_TMPDIR
TMPDIR=$TMPDIR ../../SPLIT_DIR/splitDir.sh  -s "$S3ROOT/data.tar.bz2" -d "$S3ROOT/split-data"
aws s3 cp "$S3ROOT/split-data/all-parts.lst" $TMPDIR

START_JOB_ID=$(($(date +%s) * 10000))
JOB_ARRAY_SIZE_PADDED=$(tail -1 $TMPDIR/all-parts.lst)
JOB_ARRAY_SIZE=$(($((10#$JOB_ARRAY_SIZE_PADDED)) + 1))
aws batch submit-job --cli-input-json "$(gen_json $START_JOB_ID $JOB_ARRAY_SIZE)"
# gen_json $START_JOB_ID $JOB_ARRAY_SIZE
# exit

# while read -r part;
# do
# #	https://stackoverflow.com/questions/11123717/removing-leading-zeros-before-passing-a-shell-variable-to-another-command
# 	part_numeric=$((10#$part))
# 	#JOB_ID=$(($START_JOB_ID + $part_numeric))
# 	echo "$part --> $part_numeric --> $JOB_ID"
# 	# gen_json  $JOB_ID $part
# 	aws batch submit-job --cli-input-json "$(gen_json $START_JOB_ID $part $job_array_size)"
# done < $TMPDIR/all-parts.lst
