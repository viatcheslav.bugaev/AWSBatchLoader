#!/usr/bin/env bash
# vim: ts=4:sw=4
# set -x

SCRIPT_DIR=$(dirname $(realpath "$0"))
DEBUG=no
DRY_RUN=no
BATCH_FILE_S3_URL="s3://interpreta-developer-sandbox/slava/myjob.sh"
BATCH_FILE_TYPE="script"
AWS_BATCH_JOB_ATTEMPT=1

POSITIONAL=()
while [[ $# -gt 0 ]]; do
	key="$1"

	case $key in
		-s|--src)
			SRC="$2"
			shift # past argument
			shift # past value
		;;
		-d|--dst)
			DST="$2"
			shift # past argument
			shift # past value
		;;
		--debug)
			DEBUG=yes
			shift # past argument
		;;
		--dry-run)
			DRY_RUN=yes
			shift # past argument
		;;
		*) # unknown option
			POSITIONAL+=("$1") # save it in an array for later
			shift # past argument
		;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ ${#POSITIONAL[@]} -gt 0 ]]
then
	echo "Unknown arguments: ${POSITIONAL[@]}"
	exit 1
fi

MARIADB_USER=$(aws secretsmanager get-secret-value --secret-id mariadb-user --query SecretString --output text)
MARIADB_PWD=$(aws secretsmanager get-secret-value --secret-id mariadb-pwd --query SecretString --output text)
MONGODB_ANALYTICS_KEY=$(aws secretsmanager get-secret-value --secret-id mongodb-analytics-key --query SecretString --output text)
MONGODB_USER=$(aws secretsmanager get-secret-value --secret-id mongodb-user --query SecretString --output text)

# docker run --rm -it \
# 	-e BATCH_FILE_TYPE=$BATCH_FILE_TYPE \
# 	-e BATCH_FILE_S3_URL=$BATCH_FILE_S3_URL \
# 	-e AWS_BATCH_JOB_ATTEMPT=$AWS_BATCH_JOB_ATTEMPT \
# 	-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
# 	-e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
# 	-e MARIADB_USER='{"mariadb-user": "build"}' \
# 	-e MARIADB_PWD='{"mariadb-pwd": "1terpreta"}' \
# 	-e MONGODB_ANALYTICS_KEY="$MONGODB_ANALYTICS_KEY" \
# 	610776806455.dkr.ecr.us-west-2.amazonaws.com/awsbatch/fetch_and_run_interpreta_build myjob.sh 1

docker run --rm -it \
	-e BATCH_FILE_TYPE=$BATCH_FILE_TYPE \
	-e BATCH_FILE_S3_URL=$BATCH_FILE_S3_URL \
	-e AWS_BATCH_JOB_ATTEMPT=$AWS_BATCH_JOB_ATTEMPT \
	-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
	-e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
	-e MARIADB_USER="$MARIADB_USER" \
	-e MARIADB_PWD="$MARIADB_PWD" \
	-e MONGODB_ANALYTICS_KEY="$MONGODB_ANALYTICS_KEY" \
	610776806455.dkr.ecr.us-west-2.amazonaws.com/awsbatch/fetch_and_run_interpreta_build myjob.sh 1
