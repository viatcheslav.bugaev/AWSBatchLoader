#!/usr/bin/env bash
# vim: ts=4:sw=4
# set -x

aws batch list-jobs --job-queue queue-loader --job-status runnable --output text --query jobSummaryList[*].[jobId]
