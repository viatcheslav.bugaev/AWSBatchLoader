#!/usr/bin/env bash
# vim: ts=4:sw=4
# set -x

jq \
	--arg JOB_ID 12345 \
	--arg DATADIR data \
	--arg CFGDIR cfg \
    --arg MYSQL_HOST intp-dev-uw2-mysql-01.cdh0yy3dmvtr.us-west-2.rds.amazonaws.com \
	--arg MYSQL_DB slava_sample \
	--arg NUM_THREADS 5 \
	--arg KBDIR ICLN/data/kb \
	--arg MONGO_HOST dev-cluster0-pl-0-us-west-2.x9eqz.mongodb.net \
	--arg MONGO_DB slava_sample \
	--null-input '
{
"jobName": "loader-generated",
"jobQueue": "queue-loader",
"jobDefinition": "fetch_and_run_interpreta_build_img:1",
"containerOverrides":
{
"command":
[
	"myjob.sh",
	"--rds-par",
	"--jobId=" + $JOB_ID,
	"--dataPath=" + $DATADIR,
	"--configFilesDir=" + $CFGDIR,
	"--mysqlDbName=" + $MYSQL_DB,
	"--mysqlHostName=" + $MYSQL_HOST,
	"--numThreads=" + $NUM_THREADS,
	"--mongo-par",
	"--jobId=" + $JOB_ID,
	"--kbPath=" + $KBDIR,
	"--configFilesDir=" + $CFGDIR,
	"--mysqlHostName=" + $MYSQL_HOST,
	"--mysqlDbName=" + $MYSQL_DB,
	"--mongoHostName=" + $MONGO_HOST,
    "--mongoDbName=" + $MONGO_DB,
	"--numThreads=" + $NUM_THREADS,
	"--doCompoundIndex",
	"--rerun",
	"--doUniqueIndexCheck",
	"--doCountValidation",
	"--errorPrintThreshold=20",
	"--ignoreNullUniqueKeys"

],
"environment":
[
	{
		"name": "BATCH_FILE_S3_URL",
		"value": "s3://interpreta-developer-sandbox/slava/myjob.sh"
	},
	{
		"name": "BATCH_FILE_TYPE",
		"value": "script"
	}
]
}}'
