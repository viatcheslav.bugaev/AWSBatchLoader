#!/usr/bin/env bash
# vim: ts=4:sw=4
# set -x


for i in $(aws batch list-jobs --job-queue queue-loader --job-status runnable --output text --query jobSummaryList[*].[jobId])
do
  echo "Cancel Job: $i"
  aws batch cancel-job --job-id $i --reason "Cancelling job."
  echo "Job $i canceled"
done
