#!/usr/bin/env bash
# vim: ts=4:sw=4
# set -x

SCRIPT_DIR=$(dirname $(realpath "$0"))
DEBUG=no
DRY_RUN=no
SRC="s3://interpreta-developer-sandbox/slava/myjob.sh"

POSITIONAL=()
while [[ $# -gt 0 ]]; do
	key="$1"

	case $key in
		-s|--src)
			SRC="$2"
			shift # past argument
			shift # past value
		;;
		-d|--dst)
			DST="$2"
			shift # past argument
			shift # past value
		;;
		--debug)
			DEBUG=yes
			shift # past argument
		;;
		--dry-run)
			DRY_RUN=yes
			shift # past argument
		;;
		*) # unknown option
			POSITIONAL+=("$1") # save it in an array for later
			shift # past argument
		;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ ${#POSITIONAL[@]} -gt 0 ]]
then
	echo "Unknown arguments: ${POSITIONAL[@]}"
	exit 1
fi

export BATCH_FILE_S3_URL="$SRC"
$SCRIPT_DIR/myjob.sh 1
