#!/usr/bin/env bash
# vim: ts=4:sw=4
# set -x


# Example:
#	./split-args.sh --mongo-par mx my --xxx=mInterpreta mz --rds-par --foo rx ry --bar rz --xxx rInterpreta
# 	Output:
#		RDS arguments: --foo rx ry --bar rz --xxx rInterpreta
#		MONGO arguments: mx my --xxx=mInterpreta mz
#		xxx arg is: mInterpreta
DEBUG=no
DRY_RUN=no
BATCH_SIZE=500


function peek_par_space_sep() {
	# set -x
	local PATT="$1"
	local ARR_NAME="$2"
	# Bash Array Indirection
	# https://stackoverflow.com/questions/53566026/how-can-i-reference-an-existing-bash-array-using-a-2nd-variable-containing-the-n
	# Look for the OP's solution, not the accepted one!
	local tmp="$ARR_NAME[@]"
	local ARR=("${!tmp}")
	local length=${#ARR[@]}
	for (( j=0; j<${length}; j++ ))
	do
		if [[ "$PATT" == "${ARR[$j]}" ]]
		then
			local jplus1=$((j + 1))
			echo "${ARR[$jplus1]}"
			return 0
		fi
	done
}

function peek_par_starts_with() {
	# set -x
	local PATT="$1"
	local ARR_NAME="$2"
	# Bash Array Indirection
	# https://stackoverflow.com/questions/53566026/how-can-i-reference-an-existing-bash-array-using-a-2nd-variable-containing-the-n
	# Look for the OP's solution, not the accepted one!
	local tmp="$ARR_NAME[@]"
	local ARR=("${!tmp}")
	local length=${#ARR[@]}
	for (( j=0; j<${length}; j++ ))
	do
		local PARAM="${ARR[$j]}"
		if [[ "${PARAM}" == "$PATT"* ]]
		then
			echo ${PARAM#$PATT}
			return 0
		fi
	done
}

RDS_PAR=()
MONGO_PAR=()
PAR_MODE=RDS
while [[ $# -gt 0 ]]; do
	key="$1"

	case $key in
		--rds-par)
			PAR_MODE=RDS
			shift # past argument
		;;
		--mongo-par)
			PAR_MODE=MONGO
			shift # past argument
		;;
		*) # unknown option
		if [[ $PAR_MODE == RDS ]]
		then
			RDS_PAR+=("$1") # save it in an array of parameters for customizable_mysql_loader
			shift # past argument
		else
			MONGO_PAR+=("$1") # save it in an array of parameters for customizable_mongo_loader
			shift
		fi
		;;
  esac
done


if [[ ${#RDS_PAR[@]} -gt 0 ]]
then
	echo "RDS arguments: ${RDS_PAR[@]}"
fi

if [[ ${#MONGO_PAR[@]} -gt 0 ]]
then
	echo "MONGO arguments: ${MONGO_PAR[@]}"
fi


echo "xxx arg is: $(peek_par_starts_with --xxx= MONGO_PAR)"
