#!/bin/bash
# vim: ts=4:sw=4

#TODO:

export GLOG_logtostderr=1
export GLOG_v=3

JOB_ID=$(($START_JOB_ID + $AWS_BATCH_JOB_ARRAY_INDEX))
WORKDIR=$(realpath $(pwd))
RDS_CA_BASENAME="global-bundle.pem"
CONF_BASENAME="cfg.tar.bz2"
AWS_BATCH_JOB_ARRAY_INDEX_PADDED=$(printf "%04d" $AWS_BATCH_JOB_ARRAY_INDEX)
DATA_BASENAME=$AWS_BATCH_JOB_ARRAY_INDEX_PADDED.tar.bz2
MONGO_CA_BASENAME="MongoAtlas_ISRG_Root_X1.pem"
MONGO_KEY_BASENAME="mongodb-key.pem"
export MONGOPROTOCOL=mongodb+srv
BINDIR=ICLN/bin
env


RDS_PAR=()
MONGO_PAR=()
PAR_MODE=RDS
while [[ $# -gt 0 ]]; do
	key="$1"

	case $key in
		--rds-par)
			PAR_MODE=RDS
			shift # past argument
		;;
		--mongo-par)
			PAR_MODE=MONGO
			shift # past argument
		;;
		*) # unknown option
		if [[ $PAR_MODE == RDS ]]
		then
			RDS_PAR+=("$1") # save it in an array of parameters for customizable_mysql_loader
			shift # past argument
		else
			MONGO_PAR+=("$1") # save it in an array of parameters for customizable_mongo_loader
			shift
		fi
		;;
  esac
done


if [[ ${#RDS_PAR[@]} -gt 0 ]]
then
	echo "RDS arguments: ${RDS_PAR[@]}"
fi

if [[ ${#MONGO_PAR[@]} -gt 0 ]]
then
	echo "MONGO arguments: ${MONGO_PAR[@]}"
fi


# BATCH_FILE_S3_URL is initialized by AWS Batch:
BATCH_FILE_S3_DIR_URL=${BATCH_FILE_S3_URL%/*}

function peek_par_starts_with() {
	local PATT="$1"
	local ARR_NAME="$2"
	# Bash Array Indirection
	# https://stackoverflow.com/questions/53566026/how-can-i-reference-an-existing-bash-array-using-a-2nd-variable-containing-the-n
	# Look for the OP's solution, not the accepted one!
	local tmp="$ARR_NAME[@]"
	local ARR=("${!tmp}")
	local length=${#ARR[@]}
	for (( j=0; j<${length}; j++ ))
	do
		local PARAM="${ARR[$j]}"
		if [[ "${PARAM}" == "$PATT"* ]]
		then
			echo ${PARAM#$PATT}
			return 0
		fi
	done
}


function reset_sql_db {
	mysql -u${MYSQL_USER} -p${MYSQL_PASSWD} -h${MYSQL_HOST} --ssl-ca="$RDS_CA_LOCAL" << END
	drop database if exists ${MYSQL_DB};
	create database ${MYSQL_DB};
END
}

function reset_mongo_db {
	mongo "mongodb+srv://$MONGO_HOST/?authSource=%24external&authMechanism=MONGODB-X509"  --ssl  --sslPEMKeyFile "$MONGO_KEY_LOCAL" --sslCAFile "$MONGO_CA_LOCAL" << END
	use ${MONGO_DB};
	var collections = db.getCollectionNames(); for (var i=0; i < collections.length; i++) { db[collections[i]].drop(); };
END
}

function sql_table_info {
	mysql -u${MYSQL_USER} -p${MYSQL_PASSWD} -h${MYSQL_HOST} --ssl-ca="$RDS_CA_LOCAL" << END
	SHOW GLOBAL VARIABLES LIKE 'have_ssl';
	use ${MYSQL_DB};
	show tables;
END
}

function mongo_database_info {
	echo "MONGO COLLECTIONS:"
	mongo "mongodb+srv://$MONGO_HOST/?authSource=%24external&authMechanism=MONGODB-X509"  --ssl --sslPEMKeyFile "$MONGO_KEY_LOCAL" --sslCAFile "$MONGO_CA_LOCAL" << END
	use ${MONGO_DB};
	show collections;
END
}

function mysql_info {
	mysql --version
	mysql --help
}

function host_info {
	cat /proc/cpuinfo
	whoami
}

function setup_loader_conf {
	CONF_REMOTE="${BATCH_FILE_S3_DIR_URL}/$CONF_BASENAME"
	init_CONF_LOCAL
	aws s3 cp "$CONF_REMOTE" "$CONF_LOCAL"
	tar -jxvf "$CONF_LOCAL"

}

function setup_loader_data {
	# set -x
	DATA_REMOTE="${BATCH_FILE_S3_DIR_URL}/split-data/$DATA_BASENAME"
	init_DATA_LOCAL
	aws s3 cp "$DATA_REMOTE" "$DATA_LOCAL"
	tar -jxvf "$DATA_LOCAL"

}


function install_analytics_package {
	aws s3 cp --only-show-errors s3://interpreta-build-storage/ninja/analyticsEngine-4.4.1-0-70384d3576e.tar.xz .
	tar -Jxf analyticsEngine-4.4.1-0-70384d3576e.tar.xz
	# aws s3 cp --only-show-errors s3://interpreta-build-storage/ninja/analyticsEngine-loader-hscaling-noCount-76c3bfe1309.tar.xz .
	# tar -Jxf analyticsEngine-loader-hscaling-noCount-76c3bfe1309.tar.xz
}

function is_batch {
	if [[ "x$AWS_BATCH_JOB_ATTEMPT" == x ]]
	then
		echo no
	else
		echo yes
	fi
}

function init_CONF_LOCAL {
	if [[ "$IS_BATCH" == "yes" ]]
	then
		CONF_LOCAL="/$CONF_BASENAME"
	else
		CONF_LOCAL="$(pwd)/$CONF_BASENAME"
	fi
}


function init_DATA_LOCAL {
	if [[ "$IS_BATCH" == "yes" ]]
	then
		DATA_LOCAL="/$DATA_BASENAME"
	else
		DATA_LOCAL="$(pwd)/$DATA_BASENAME"
	fi
}

function init_RDS_CA_LOCAL {
	if [[ "$IS_BATCH" == "yes" ]]
	then
		RDS_CA_LOCAL="/$RDS_CA_BASENAME"
	else
		RDS_CA_LOCAL="$(pwd)/$RDS_CA_BASENAME"
	fi
}

function init_MONGO_CA_LOCAL {
	if [[ "$IS_BATCH" == "yes" ]]
	then
		MONGO_CA_LOCAL="/$MONGO_CA_BASENAME"
	else
		MONGO_CA_LOCAL="$(pwd)/$MONGO_CA_BASENAME"
	fi
}

function init_MONGO_KEY_LOCAL {
	if [[ "$IS_BATCH" == "yes" ]]
	then
		MONGO_KEY_LOCAL="/$MONGO_KEY_BASENAME"
	else
		MONGO_KEY_LOCAL="$(pwd)/$MONGO_KEY_BASENAME"
	fi
}

function setup_RDS {
	MYSQL_DB=$(peek_par_starts_with --mysqlDbName= RDS_PAR)
	MYSQL_HOST=$(peek_par_starts_with --mysqlHostName= RDS_PAR)
	init_RDS_CA_LOCAL
	RDS_CA_REMOTE="${BATCH_FILE_S3_DIR_URL}/$RDS_CA_BASENAME"

	if [[ "$IS_BATCH" == "no" ]]
	then
		MYSQL_USER=$(aws secretsmanager get-secret-value --secret-id mariadb-user --query SecretString --output text | jq --raw-output '."mariadb-user"')
		MYSQL_PASSWD=$(aws secretsmanager get-secret-value --secret-id mariadb-pwd --query SecretString --output text | jq --raw-output '."mariadb-pwd"')
	else
		MYSQL_USER=$(echo "$MARIADB_USER" | jq --raw-output '."mariadb-user"')
		MYSQL_PASSWD=$(echo "$MARIADB_PWD" | jq --raw-output '."mariadb-pwd"')
	fi

	aws s3 cp "$RDS_CA_REMOTE" "$RDS_CA_LOCAL"
}

function setup_MONGO {
	MONGO_DB=$(peek_par_starts_with --mongoDbName= MONGO_PAR)
	init_MONGO_CA_LOCAL
	MONGO_CA_REMOTE="${BATCH_FILE_S3_DIR_URL}/$MONGO_CA_BASENAME"
	init_MONGO_KEY_LOCAL
	# There is no MONGO_KEY_REMOTE.  We take the key from an environment variable.

	MONGO_HOST="$(peek_par_starts_with --mongoHostName= MONGO_PAR)"
	if [[ "$IS_BATCH" == "no" ]]
	then
		MONGO_USER=$(aws secretsmanager get-secret-value --secret-id mongodb-user --query SecretString --output text | jq --raw-output '."mongodb-user"')
		MONGO_PASSWD=$(aws secretsmanager get-secret-value --secret-id mongodb-pwd --query SecretString --output text | jq --raw-output '."mongodb-pwd"')
		aws secretsmanager get-secret-value --secret-id mongodb-analytics-key --query SecretString --output text > "$MONGO_KEY_LOCAL"
	else
		MONGO_USER=$(echo "$MONGODB_USER" | jq --raw-output '."mongodb-user"')
		MONGO_PASSWD=$(echo "$MONGODB_PWD" | jq --raw-output '."mongodb-pwd"')
		echo "$MONGODB_ANALYTICS_KEY" > "$MONGO_KEY_LOCAL"
	fi

# HACK ALERT: Fix the MONGO_USER in SECRETS, Remove the assignment for MONGO_USER below.
	MONGO_USER="CN=Dev-AtlasUser-RW"
	echo aws s3 cp "$MONGO_CA_REMOTE" "$MONGO_CA_LOCAL"
	aws s3 cp "$MONGO_CA_REMOTE" "$MONGO_CA_LOCAL"
}

IS_BATCH=$(is_batch)

set -x

# echo pwd: "$(pwd)"
# echo "Args: $@"
#env
echo "THE TOP OF LOG OUTPUT"
echo "jobId: $AWS_BATCH_JOB_ID"


setup_RDS
# reset_sql_db

setup_loader_conf
setup_loader_data

install_analytics_package

echo Parameters given: "${RDS_PAR[@]}"

${BINDIR}/customizable_mysql_loader \
	"${RDS_PAR[@]}" \
    --mysqlUserName=${MYSQL_USER} \
    --mysqlPassword=${MYSQL_PASSWD} \
	--dataPath=$AWS_BATCH_JOB_ARRAY_INDEX_PADDED \
	--jobId=$JOB_ID

echo "bye bye!!"
