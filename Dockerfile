FROM 610776806455.dkr.ecr.us-west-2.amazonaws.com/build/int-build-image:latest
ADD fetch_and_run.sh /usr/local/bin/fetch_and_run.sh
ADD add-mongo-repo.sh .
RUN ./add-mongo-repo.sh
RUN yum -y install mongodb-org-shell 
RUN yum -y install jq
WORKDIR /tmp
# USER nobody
ENTRYPOINT ["/usr/local/bin/fetch_and_run.sh"]
